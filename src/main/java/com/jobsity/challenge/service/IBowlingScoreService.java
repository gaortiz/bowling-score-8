package com.jobsity.challenge.service;

import com.jobsity.challenge.model.PlayerShots;

import java.util.List;

public interface IBowlingScoreService {

    void calculateScore(final List<PlayerShots> playersShots);

}
