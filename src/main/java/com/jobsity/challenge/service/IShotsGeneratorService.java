package com.jobsity.challenge.service;

import com.jobsity.challenge.model.Shot;

import java.util.List;

public interface IShotsGeneratorService {

    List<Shot> getShotSequenceForProcessing(final List<String> shots, final int shotIndex);

}
